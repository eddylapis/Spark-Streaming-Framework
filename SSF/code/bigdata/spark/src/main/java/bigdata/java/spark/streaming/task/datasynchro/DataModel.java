package bigdata.java.spark.streaming.task.datasynchro;

import bigdata.java.framework.spark.util.JsonUtil;
import bigdata.java.framework.spark.util.StrUtil;
import scala.Tuple3;

import java.util.List;

public class DataModel {

    /**
     * 表名
     */
    String table;
    /**
     * 主键
     */
    List<Tuple3<String,String,String>> primary_Keys;
    /**
     * 字段
     */
    List<Tuple3<String,String,String>> fields;
    /**
     * 操作类型
     */
    String op_Type;
    /**
     * sql语句
     */
    String sql;

    String json;

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public List<Tuple3<String, String, String>> getFields() {
        return fields;
    }

    public Tuple3<String, String, String> getPrimaryValue(String key)
    {
        for (int i = 0; i < primary_Keys.size(); i++) {
            Tuple3<String, String, String> tuple3 = primary_Keys.get(i);
            if(tuple3._1().equals(key))
            {
                return tuple3;
            }
        }
        return null;
    }

    public Tuple3<String, String, String> getFieldValue(String key)
    {
        for (int i = 0; i < fields.size(); i++) {
            Tuple3<String, String, String> tuple3 = fields.get(i);
            if(tuple3._1().equals(key))
            {
                return tuple3;
            }
        }
        return null;
    }

    public void setFields(List<Tuple3<String, String, String>> fields) {
        this.fields = fields;
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public List<Tuple3<String,String,String>> getPrimary_Keys() {
        return primary_Keys;
    }

    public void setPrimary_Keys(List<Tuple3<String,String,String>> primary_Keys) {
        this.primary_Keys = primary_Keys;
    }

    public String getOp_Type() {
        return op_Type;
    }

    public void setOp_Type(String op_Type) {
        this.op_Type = op_Type;
    }

    /**类型转换
     * @param typeName
     * @return
     */
    public static String covertType(String typeName)
    {
        String type="";
        if(typeName.contains("String"))
        {
            type="String";
        }
        else if(typeName.contains("Integer"))
        {
            type="Integer";
        }
        else if(typeName.contains("Boolean"))
        {
            type="Boolean";
        }
        else if(typeName.contains("Long"))
        {
            type="Long";
        }
        else if(typeName.contains("Double"))
        {
            type="Double";
        }
        else if(typeName.contains("BigDecimal"))
        {
            type="BigDecimal";
        }
        else
        {
            type="";
//                            continue;
        }
        return type;
    }

    /**根据类型，生成对于的数据库插入value是否带有引号
     * @param value
     * @param type
     * @return
     */
    public String getTyepAferValue(String value,String type)
    {
        String afterValue = "";
        if(type.contains("String"))
        {
            afterValue="\""+value+"\"";
        }
        else if(type.contains("Integer"))
        {
            afterValue= value;
        }
        else if(type.contains("Boolean"))
        {
            afterValue= value;
        }
        else if(type.contains("Long"))
        {
            afterValue= value;
        }
        else if(type.contains("Double"))
        {
            afterValue= value;
        }
        else if(type.contains("BigDecimal"))
        {
            afterValue= value;
        }
        else
        {
            afterValue="";
        }
        return afterValue;
    }

    public String getSql() {
        return sql;
    }

    /**
     * 生成sql语句
     */
    public void createSql() {
        //获取条件，根据主键
        String where ="";
        if(getPrimary_Keys().size() < 1)
        {//没有发现主键，那么直接报错
            throw new RuntimeException("no find primary key");
        }
        for (int i = 0; i < getPrimary_Keys().size(); i++) {
            Tuple3<String, String, String> tuple3 = getPrimary_Keys().get(i);
            String key = tuple3._1();
            String value = tuple3._2();
            String type = tuple3._3();
            where += " " + key + " = " +getTyepAferValue(value,type) + " and";
        }
        //移除最后一个and字符串
        int and = where.lastIndexOf("and");
        where = where.substring(0,and);

        if(getOp_Type().equals(JsonUtil.DELETE))
        {//删除操作
            sql = "delete from " + getTable() + " where " + where;
        }
        else if(getOp_Type().equals(JsonUtil.UPDATE))
        {//更新操作
            sql = "update "+getTable()+" set ";
            for (int i = 0; i < getFields().size(); i++) {
                Tuple3<String, String, String> tuple3 = getFields().get(i);
                String key = tuple3._1();
                String value = tuple3._2();
                String type = tuple3._3();
                sql+=  key+"=" + getTyepAferValue(value,type)+",";
            }
            sql = StrUtil.removeLastChar(sql);
            sql += " where " +  where;
        }
        else if(getOp_Type().equals(JsonUtil.INSERT))
        {//新增操作
            String fieldString="";
            String valueString="";
            sql = "insert into "+getTable()+" ";
            //主键字段
            for (int i = 0; i < getPrimary_Keys().size(); i++) {
                Tuple3<String, String, String> tuple3 = getPrimary_Keys().get(i);
                String key = tuple3._1();
                String value = tuple3._2();
                String type = tuple3._3();
                fieldString +=  key + ",";
                valueString +=   getTyepAferValue(value,type) + ",";
            }

            //普通字段生成sql拼接
            for (int i = 0; i < getFields().size(); i++) {
                Tuple3<String, String, String> tuple3 = getFields().get(i);
                String key = tuple3._1();
                String value = tuple3._2();
                String type = tuple3._3();
                fieldString +=  key + ",";
                valueString +=   getTyepAferValue(value,type) + ",";
            }

            fieldString = StrUtil.removeLastChar(fieldString);
            valueString = StrUtil.removeLastChar(valueString);
            sql+= "("+fieldString+")values("+valueString+")";
        }
    }
}
