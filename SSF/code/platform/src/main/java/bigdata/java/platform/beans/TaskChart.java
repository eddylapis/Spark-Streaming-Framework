package bigdata.java.platform.beans;

import javafx.concurrent.Task;

import java.util.Date;

public class TaskChart {
    Integer taskId;
    Long consumeOffset;
    Long maxOffset;
    String topic;
    Date uptime;

    public Integer getTaskId() {
        return taskId;
    }

    public void setTaskId(Integer taskId) {
        this.taskId = taskId;
    }

    public Long getConsumeOffset() {
        return consumeOffset;
    }

    public void setConsumeOffset(Long consumeOffset) {
        this.consumeOffset = consumeOffset;
    }

    public Long getMaxOffset() {
        return maxOffset;
    }

    public void setMaxOffset(Long maxOffset) {
        this.maxOffset = maxOffset;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Date getUptime() {
        return uptime;
    }

    public void setUptime(Date uptime) {
        this.uptime = uptime;
    }

    @Override
    public boolean equals(Object obj) {
        if(this ==obj)
        {
            return true;
        }
        if(obj ==null)
        {
            return false;
        }
        if (obj instanceof  TaskChart)
        {
            TaskChart taskChart = (TaskChart)obj;
            if(taskChart.getTopic().equals(this.getTopic()))
            {
                return true;
            }
        }
        return false;
    }

    @Override
    public int hashCode() {
        return getTopic().hashCode();
    }
}
