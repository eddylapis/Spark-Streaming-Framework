/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util;

import com.typesafe.config.Config;
import org.apache.commons.lang.StringUtils;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

/**
*redis连接工具类
 * @deprecated 已废弃，建议使用bigdata.java.framework.spark.util.client.JedisClient
*/
@Deprecated
public class JedisUtil {

    private static JedisUtil instance=null;

    /**
     * 获取工具类实例对象
     */
    public static JedisUtil getInstance(){
        if(instance == null){
            synchronized(JedisUtil.class){
                if(instance==null){
                    instance = new JedisUtil();
                }
            }
        }
        return instance;
    }

    private JedisPool jedisPool = null;

    private JedisUtil() {
        JedisPoolConfig config = new JedisPoolConfig();
        //最大连接数
        config.setMaxTotal(20);
        //最大空闲的连接数
        config.setMaxIdle(5);
        //最小空闲的连接数
        config.setMinIdle(2);
        //当连接池用尽后，调用者的最大等待时间（单位毫秒）
        config.setMaxWaitMillis(10000);
        //向连接池借用连接时是否做连接有效性检测（ping）
        config.setTestOnBorrow(true);
        //向连接池归还连接时是否做连接有效性检测（ping）
        config.setTestOnReturn(true);
        //向连接池借用连接时是否做连接空闲检测
        config.setTestWhileIdle(true);
        //空闲连接的检测周期（单位毫秒），默认-1表示不做检测
        config.setTimeBetweenEvictionRunsMillis(30000);
        //做空闲连接检测时，每次的采样数，默认3
        config.setNumTestsPerEvictionRun(3);
        //连接的最小空闲时间，达到此值后空闲连接将被移除
        config.setMinEvictableIdleTimeMillis(60000);

        String server ="";
        Integer port = 0;
        Config applicationConfig = ConfigUtil.config;
        if(applicationConfig.hasPath("redis.servers"))
        {
            String redisList = applicationConfig.getString("redis.servers");
            if(StringUtils.isNotEmpty(redisList))
            {
                String[] split = redisList.split(":");
                if(split.length!=2)
                {
                    throw new RuntimeException("redis.servers config error");
                }
                server = split[0];
                port = Integer.valueOf(split[1]);
            }
            else
            {
                throw new RuntimeException("redis no config");
            }
        }
        boolean hasPWD = applicationConfig.hasPath("redis.password");
        if(hasPWD)
        {
            String pwd = applicationConfig.getString("redis.password");
            if(org.apache.commons.lang3.StringUtils.isBlank(pwd))
            {//密码为空，那么认定为没有密码
                jedisPool = new JedisPool(config, server, port, 10000);
            }
            else
            {//有密码
                jedisPool = new JedisPool(config, server, port, 10000,pwd);
            }
        }
        else
        {//配置文件中不包含redis.password，认定为没有密码
            jedisPool = new JedisPool(config, server, port, 10000);
        }
    }

    /**
     从连接池中获取jedis对象
     */
    public Jedis getJedis() {
        Jedis jedis = null;
        try {
            jedis = jedisPool.getResource();
        } catch (Exception e) {
            jedis.close();
        }
        return jedis;
    }

    /**
     * 根据key获取value
     * @param key key 字符串
     * @return 字符串
     */
    public String get(String key)
    {
        Jedis jedis = null;
        String value=null;
        try
        {
            jedis = getJedis();
            value = jedis.get(key);
        }
        catch (Exception e)
        {
            if(jedis!=null)
            {
                jedis.close();
            }
        }
        return value;
    }

    /**
     * 根据key 设置对应的value值
     * @param key key 字符串
     * @param value value 字符串
     */
    public String set(String key,String value)
    {
        Jedis jedis = null;
        String returnValue=null;
        try
        {
            jedis = getJedis();
            returnValue = jedis.set(key,value);
        }
        catch (Exception e)
        {
            if(jedis!=null)
            {
                jedis.close();
            }
        }
        return returnValue;
    }

}
