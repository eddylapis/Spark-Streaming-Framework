package bigdata.java.framework.spark.util.client;

import bigdata.java.framework.spark.pool.sql.SqlConfig;
import bigdata.java.framework.spark.pool.sql.SqlConnectionPool;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Mysql客户端工具类
 */
public class SqlClient {

    private static SqlClient instance=null;
    /**
     * 获取工具类实例对象
     */
    public static SqlClient getInstance(){
        if(instance == null){
            synchronized(SqlClient.class){
                if(instance==null){
                    instance = new SqlClient();
                }
            }
        }
        return instance;
    }

    private SqlConnectionPool mySqlConnectionPool = null;

    public SqlConnectionPool getConnectionPool() {
        return mySqlConnectionPool;
    }

    private SqlClient() {
        SqlConfig mySqlConfig = new SqlConfig();
        mySqlConnectionPool = new SqlConnectionPool(mySqlConfig);
    }

    /**归还链接
     * @param pool  连接池对象
     * @param resource 连接对象
     * @param preparedStatement preparedStatement
     * @param rs ResultSet
     */
    public static void returnConnection(SqlConnectionPool pool, Connection resource, PreparedStatement preparedStatement, ResultSet rs){

        if(null != rs){
            try {
                rs.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if (null != preparedStatement){
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if(pool!=null && resource!=null)
        {
            pool.returnConnection(resource);
        }
    }

    /**
     * 判断数据库是否支持批处理
     */
    public Boolean supportBatch()
    {
        SqlConnectionPool pool = null;
        Connection resource = null; // 连接
        Boolean supportsBatchUpdates=false;
        try {
            pool = getConnectionPool();
            resource = pool.getConnection();
            DatabaseMetaData metaData = resource.getMetaData();
            supportsBatchUpdates = metaData.supportsBatchUpdates();
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
        finally {
            returnConnection(pool,resource,null,null);
        }
        return supportsBatchUpdates;
    }

    /**
     * ResultSet转换为 List
     * @param rs ResultSet
     * @return map list
     */
    public List<Map<String, Object>> selectAll(ResultSet rs) {
        List<Map<String, Object>> list = new ArrayList<Map<String, Object>>();
        if(rs==null)
        {
            return list;
        }
        try {
            // 获取结果集结构（元素据）
            ResultSetMetaData rmd = rs.getMetaData();
            // 获取字段数（即每条记录有多少个字段）
            int columnCount = rmd.getColumnCount();
            while (rs.next()) {
                // 保存记录中的每个<字段名-字段值>
                Map<String, Object> rowData = new HashMap<String, Object>();
                for (int i = 1; i <= columnCount; ++i) {
                    // <字段名-字段值>
                    rowData.put(rmd.getColumnName(i), rs.getObject(i));
                }
                // 获取到了一条记录，放入list
                list.add(rowData);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    /**
     * 执行查询sql语句
     * @param sql sql语句
     * @return 返回结果集list
     */
    public List<Map<String, Object>> QuerySQL(String sql)
    {
        // 定义结果集
        ResultSet rs = null;
        SqlConnectionPool pool = null;
        Connection resource = null; // 连接
        PreparedStatement pstm  = null; //
        List<Map<String, Object>> maps = new ArrayList<>();
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            System.out.println(resource);
            // 执行SQL 语句, 返回结果集
            pstm = resource.prepareStatement(sql);
            rs   = pstm.executeQuery();
            maps = selectAll(rs);
        } catch (SQLException e){
            throw new RuntimeException(e);
        }finally {
            returnConnection(pool,resource,pstm,rs);
        }
        return maps;
    }

    /**
     * 批量执行sql （带事物）
     * @param list 批量sql语句list
     */
    public Boolean ExecuteSqlList(List<String> list)
    {
        SqlConnectionPool pool = null;
        Connection resource = null; // 连接
        PreparedStatement pstm  = null; //
        Boolean success=false;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            resource.setAutoCommit(false);

            for (int i = 0; i < list.size(); i++) {
                String sql = list.get(i);
                pstm = resource.prepareStatement(sql);
                pstm.executeUpdate();
            }
            resource.commit();
            success = true;
        } catch (SQLException e){
            try {
                if(resource!=null)
                {
                    resource.rollback();
                }
            } catch (SQLException e1) {
                throw new RuntimeException(e1);
            }
            throw new RuntimeException(e);
        } finally{

            try {
                resource.setAutoCommit(true);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            returnConnection(pool,resource,pstm,null);
        }
        return success;
    }

    /**
     * 执行增，删，改sql语句
     * @param sql 执行的sql语句
     * @return 影响的行数
     */
    public int ExecuteSQL(String sql)
    {
        SqlConnectionPool pool = null;
        Connection resource = null; // 连接
        PreparedStatement pstm  = null; //
        int code = 0;
        try{
            pool = getConnectionPool();
            resource = pool.getConnection();
            pstm = resource.prepareStatement(sql);
            code = pstm.executeUpdate();
//            connection.commit();
        } catch (SQLException e){
            throw new RuntimeException(e);
        } finally{
            returnConnection(pool,resource,pstm,null);
        }
        return code;
    }

    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Thread(() -> {
                SqlClient.getInstance().QuerySQL("select count(1) from Task");
            }).start();
        }

    }
}
