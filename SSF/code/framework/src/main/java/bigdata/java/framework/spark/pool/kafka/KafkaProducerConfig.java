/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.pool.kafka;

import bigdata.java.framework.spark.pool.ConnectionPoolConfig;

import java.util.Properties;

public class KafkaProducerConfig extends ConnectionPoolConfig {
    Properties properties = new Properties();
    Properties poolProperties = new Properties();
    public KafkaProducerConfig()
    {
        init();
    }
    public void init()
    {
        properties = ConnectionPoolConfig.getProperties("kafkaproducerparam.");
        poolProperties = ConnectionPoolConfig.getProperties("kafkaproducer.pool.");
        ConnectionPoolConfig.setPoolProperties(this,poolProperties);
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
