/**
 * Apache License V2.0
 * Copyright (c) 2019-2019 bin (10112005@qq.com)
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package bigdata.java.framework.spark.util.es5;

import bigdata.java.framework.spark.util.ConfigUtil;
import com.typesafe.config.Config;
import com.typesafe.config.ConfigValue;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.PooledObjectFactory;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.transport.client.PreBuiltTransportClient;

import java.net.InetAddress;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

public class ElasticSearchClientFactory implements PooledObjectFactory<TransportClient> {

    private AtomicReference<Set<HostAndPort>> nodesReference = new AtomicReference<Set<HostAndPort>>();

    private String clusterName;

    public ElasticSearchClientFactory(String clusterName, Set<HostAndPort> clusterNodes){
        this.clusterName = clusterName;
        this.nodesReference.set(clusterNodes);
    }

    public PooledObject<TransportClient> makeObject() throws Exception {
        try
        {
            Config config = ConfigUtil.getConfig();
            Settings.Builder settings = Settings.builder();
            Stream<Map.Entry<String, ConfigValue>> filterConfig = config.entrySet().stream()
                    .filter(e -> e.getKey().startsWith("esparam."));
            filterConfig.forEach(e -> settings.put(
                    e.getKey().replace("esparam.", "")
                    , config.getString(e.getKey())));
            Settings build = settings.build();
            PreBuiltTransportClient client = new PreBuiltTransportClient(build);
            for (HostAndPort hostAndPort : nodesReference.get()) {
                client.addTransportAddress(new InetSocketTransportAddress(InetAddress.getByName(hostAndPort.getHost()), hostAndPort.getPort()));
            }
            return new DefaultPooledObject(client);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public void destroyObject(PooledObject<TransportClient> pooledObject) throws Exception {
        TransportClient client = pooledObject.getObject();
        if (client != null) {
            try {
                client.close();
            } catch (Exception e) {
                //ignore
            }
        }
    }

    public boolean validateObject(PooledObject<TransportClient> pooledObject) {
        TransportClient client = pooledObject.getObject();
        try {
            client.connectedNodes();
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public void activateObject(PooledObject<TransportClient> pooledObject) throws Exception {
        TransportClient client = pooledObject.getObject();
        client.connectedNodes();
    }

    public void passivateObject(PooledObject<TransportClient> pooledObject) throws Exception {
        //nothing
    }


}
